README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Czech, edition 1.3. See the wiki pages of the [PARSEME corpora initiative](https://gitlab.com/parseme/corpora/-/wikis/home) for the full documentation of the annotation principles.

The present Czech data result from an update of the Czech part of the [PARSEME corpus v 1.0](http://hdl.handle.net/11372/LRT-2282?show=full).
For the changes with respect to version 1.0, see the change log below.

Source corpora
-------
All annotated data come from one of these newspapers:
1. Lidové noviny (daily newspapers), 1991, 1994, 1995
2. Mladá fronta Dnes (daily newspapers), 1992
3. Českomoravský Profit (business weekly), 1994
4. Vesmír (scientific journal), 1992, 1993


Format
-----------------
* columns 1-3 (ID, FORM, LEMMA) : Available, kept from version 1.0 of the corpus, where they were manually annotated)
* columns 4-10 (UPOS, XPOS, FEATS, HEAD, DEPREL, DEPS, MISC): Available, automatically annotated by [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) (model czech-pdt-ud-2.10-220711). This implies:
  * in column 4 (UPOS): the [UD POS-tags](http://universaldependencies.org/u/pos) (as of January 2023), 
  * in column 6 (FEATS): [UD features](http://universaldependencies.org/u/feat/index.html) (as of January 2023)
  * in column 8 (DEPREL): [UD dependency relations](http://universaldependencies.org/u/dep) (as of January 2023)
* column 11 (PARSEME:MWE): Available, manually annotated (in edition 1.0); the tagset uses the [PARSEME VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) version 1.3

Tokenization
------------
* Hyphens: Always split as a single token, resulting in three tokens out of "chceme-li".
* Contractions: Most contractions are kept as a single unit (not-split).

Annotation
----------
VMWEs in this language are annotated for the following categories: VID, LVC.full, IRV.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 


Authors
----------
All VMWEs annotations were performed by Alla Bémová, Eva Buráňová,
Jakub Dotlačil, Milena Hnátková, Natalia Klyueva, Marie Mikulová,
Kateřina Součková, Magda Ševčíková, Pavel Šidák, Jana Šindlerová,
Eva Šťastná, Zdeňka Urešová, Pavlína Vimmrová, Eduard Bejček,
Petr Pajas, and Pavel Straňák.

License
---------- 
The data are distributed under the terms of the Creative Commons license
([CC-BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/)).

Contact
----------
* bejcek@ufal.mff.cuni.cz
* agata.savary@universite-paris-saclay.fr

Future work
-----------
Manual morphosyntactic annotations are probably available in the source corpora. If so, they should be converted to UD 2 tagsets instead of being replaced with autompatic UDPipe annotation. 
This action might be non trivial because the original PARSEME 1.0 corpus for Czech does not contain source sentence identifiers.

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - The 2 expressions annotated OTH in version 1.0 and renamed to TODO in the previous update were annotated as VID:
  	- _Psi štěkají, karavana jde dál_
  	- _jak se sluší a patří_
  - The UD-compatible morphosyntactic annotations columns (UPOS to MISC) were generated with UDPipe (czech-pdt-ud-2.10-220711 model)
  - Contact for this update: Agata Savary
- **2022-03**:
  - The corpus in version 1.0 was automatically converted from the [.parseme-tsv](https://typo.uni-konstanz.de/parseme/index.php/2-general/184-parseme-shared-task-format-of-the-final-annotation) format (with aligned syntactic annotations in [.conllu](https://universaldependencies.org/format.html)) to the [.cupt](https://multiword.sourceforge.net/cupt-format) format.
  - The only 2 VMWEs tagged with the OTH category were renamed to TODO. 
  - For the 1.0 to 1.2 conversion to be complete, the following manual updates are necessary:
    - Rename expressions marked with TODO to one of the existing categories (likely a VID) - done on 2022-08-30
    - Find all expressions which pass the LVC.cause tests - still **to do**
    - See also [what is new in the annotation guidelines version 1.1 with respect to 1.0](https://docs.google.com/document/d/15XPEYCK7tE9pTO1yjaqi_oQCtFX_HRszMxCNGjwIDFI/edit?usp=sharing) and in [version 1.2 with respect to 1.1](https://docs.google.com/document/d/1ukhPgfai6kmP5ugTjGbfhE0Wo3GqkYSAtYnnZG6bDgw/edit#heading=h.r4tu3hytzywq)
  - Contact for this update: Carlos Ramisch
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT. 
  
